#include "global.h"


using namespace StrUtil;
passwd_t *users;

void initDb() {
	FILE *f = fopen("base_2.bin", "r");
	char *buf;
	long len = getLength(f);
	buf = (char*)malloc(len);
	fread(buf, 1, len, f);
	buf = strenc(buf);
	buf[len - 2] = 0;
	int lines = countLines(buf, len);
	//printf("lines: %d\n", lines);
	users = (passwd_t*)malloc(lines * sizeof(passwd_t));
	for (int i = 0; i < lines; i++) {
		char *_line = getLine(buf, i);
		strcpy(users[i].pw_name, getData(_line, USERNAME));
		strcpy(users[i].pw_passwd, getData(_line, PASSWORD));
		strcpy(users[i].pw_realname, getData(_line, REALNAME));
		printf("%s\t%s\t%s\n", users[i].pw_name, users[i].pw_passwd, users[i].pw_realname);
	}
	fclose(f);
}
int main() {
	/*printf("-----testing...-----\n");
	char *test = "Kappa123";
	printf("%s\n%s\n", strenc(test), strenc(strenc(test)));
	printf("!!---testing...---!!\n");*/
	initDb();
	for (int trys = 0; trys < 3; trys++) {
		loginMenu();
	}

	//getchar();
	system("pause");
	return 0;
}