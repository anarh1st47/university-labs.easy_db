#include "util.h"

long StrUtil::getLength(FILE *f) {
	fseek(f, 0, SEEK_END);
	long len = ftell(f);
	fseek(f, 0, SEEK_SET);
	return len;
}

int StrUtil::countLines(char data[], int len) {
	int lines = 1;
	for (int i = 0; i < len; i++)
		if (data[i] == '\n' || i == len - 1)
			lines++;
	return lines - 1;
}

char *StrUtil::split(char *data, char del, int number) {
	int copyFrom = 0, curNumber = 0;
	//printf("splitting by %d..\n%s\n\n", del,  data);
	int i = 0;
	//for (i = 0; i < len; i++) {
	while (data[i] != 0) {
		//printf("%d\n", i);
		if (data[i] == '\0') break;
		if (data[i] == del || data[i] == '\n') { //sad but no idea FIXME:
												 //printf("taaaak, vrode norm.. del is %d and i is %d, curNumber is %d\n", del, i, curNumber);
			if (number == curNumber) {
				//printf("ok! returning..\n");
				char *res = (char*)malloc(i - copyFrom + 1);
				memcpy(res, data + copyFrom, i - copyFrom);
				res[i - copyFrom] = 0;
				return res;
			}
			else {
				++curNumber;
				copyFrom = i + 1;
			}
		}
		i++;
	}
	char *res = (char*)malloc(i - copyFrom + 1);
	memcpy(res, data + copyFrom, i - copyFrom);
	res[i - copyFrom] = 0;
	return res;
}

char* StrUtil::getLine(char data[], int number) {
	return split(data, '\n', number);
}


char* StrUtil::getData(char data[], int number) {
	return split(data, ':', number);
}



void Util::saveStruct(char *_pass) {
	FILE *f = fopen("base_2.bin", "w");
	//fread(buf, 1, len, f);
	int _len = sizeof(passwd_t)+strlen(_pass);
	char *buf = (char*)malloc(_len);
	strcpy(buf, "");
	for (int i = 0; i < sizeof(users) - 1; i++) {
		strcat(buf, users[i].pw_name);
		strcat(buf, ":");
		strcat(buf, users[i].pw_passwd);
		strcat(buf, ":");
		strcat(buf, users[i].pw_realname);
		strcat(buf, "\n");
	}
	strcat(buf, "\0");
	//buf = "hujpizda";
	_asm nop;
	//printf("%s\n\n", buf);
	printf("%d\n", sizeof(users));
	fwrite(strenc(buf), 1, _len, f);
	fclose(f);
}