#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "global.h"

namespace StrUtil {
	long getLength(FILE *);
	int countLines(char data[], int len);
	char *split(char * data, char del, int number);
	char *getLine(char data[], int number);
	char *getData(char data[], int number);
	enum {
		USERNAME,
		PASSWORD,
		REALNAME
	};
}

namespace Util{
	void saveStruct(char*);
}